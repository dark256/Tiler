/**
 * Created by Root on 12.12.17.
 */
package tools {

    import flash.display.Sprite;
    import flash.geom.ColorTransform;

    public class AdjustColor {

        public static function setDarkness( _target:Sprite, val:Number ):void {
            var _val:Number = val;
            _target.transform.colorTransform = new ColorTransform(1, 1, 1, 1, _val, _val, _val, 0);
        }
    }
}
