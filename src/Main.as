package {

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import tiles.ObjectPool;
    import tiles.TileManager;

    import tiles.TileType;


    [SWF(width='600', height='600', backgroundColor='0x333333', frameRate='60')]

    public class Main extends Sprite {

        private var _mainScreen:MainScreen;
        private var _tileManager:TileManager;


        public function Main() {
            if ( stage ) {
                initApp();
            } else {
                addEventListener( Event.ADDED_TO_STAGE, onAddToStage );
            }
        }

        private function onAddToStage( e:Event ):void {
            removeEventListener( Event.ADDED_TO_STAGE, onAddToStage );
            initApp();
        }

        private function initApp():void {

            addChild( new Stats() );

            _mainScreen  = addChild( new MainScreen() ) as MainScreen;
            _mainScreen.x = 50;

                        //---------------------------------------------------------------
                        _tileManager = new TileManager( _mainScreen, _mainScreen.viewPort, 20 );

                            _tileManager.addTiles( TileType.GROUND, 1 );        // Классы отображения
                            _tileManager.addTiles( TileType.SKY, 10  );
                            //_tileManager.addTiles( TileType.STRATOSPHERE, 2 );
                            //_tileManager.addTiles( TileType.SPACE, 3 );
                            _tileManager.init();

                        _tileManager.drawViewPort(0);                            // Отрисовать с позиции такой-то
                        //---------------------------------------------------------------


            // Девелоперский контроллер
            _mainScreen.upBtn.addEventListener( MouseEvent.MOUSE_OVER, shiftUp);
            _mainScreen.upBtn.addEventListener( MouseEvent.MOUSE_OUT, shiftOff);
            _mainScreen.dnBtn.addEventListener( MouseEvent.MOUSE_OVER, shiftDn);
            _mainScreen.dnBtn.addEventListener( MouseEvent.MOUSE_OUT, shiftOff);
            _mainScreen.clearPool.addEventListener( MouseEvent.CLICK, onClearPool );
        }

        private var inc:int = 5;
        private function shiftOff( event:MouseEvent ):void {
            this.removeEventListener( Event.ENTER_FRAME, onEnterFrameUp );
            this.removeEventListener( Event.ENTER_FRAME, onEnterFrameDn );
        }
        private function shiftUp( event:MouseEvent ):void {
            this.addEventListener( Event.ENTER_FRAME, onEnterFrameUp );
        }
        private function shiftDn( event:MouseEvent ):void {
            this.addEventListener( Event.ENTER_FRAME, onEnterFrameDn );
        }
        private function onEnterFrameUp( event:Event ):void {
            _tileManager.drawViewPort(inc);
        }
        private function onEnterFrameDn( event:Event ):void {
            _tileManager.drawViewPort(-inc);
        }
        private function onClearPool( event:MouseEvent ):void {
            ObjectPool.purge();
        }
    }
}
