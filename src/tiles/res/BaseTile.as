/**
 * Created by Root on 05.12.17.
 */
package tiles.res {

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Sprite;

    public class BaseTile extends Sprite{

        protected var _tile:SingleTile;
        protected var _id:int;

        public function BaseTile( id:int ) {
            _id = id;
            _tile = new SingleTile();
            addChild( _tile );
            draw();
        }

        public function draw():void {
            _tile.desc.text = 'SingleTile';
        }

        public function addInfo( val:String ):void{
            _tile.idx.text = val;
        }

        public function drawBack( picture:Class ):void {
            if ( picture ) {
                var pict:BitmapData = new picture();
                var bmp:Bitmap      = new Bitmap( pict, "auto", true );
                _tile.pict.addChild( bmp );
            }
        }

        public function purge():void{
            var target:Sprite = _tile.pict;
            var child:Object;
            for ( var i:int = 0; i < target.numChildren; i++ ) {
                child = target.getChildAt( i );
                if ( child is Bitmap ) {
                    ( child as Bitmap).bitmapData.dispose();
                }
            }
        }

        public function destroy():void{
            this.parent.removeChild( this );
        }

    }
}
