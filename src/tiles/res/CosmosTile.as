/**
 * Created by Root on 05.12.17.
 */
package tiles.res {

    public class CosmosTile extends BaseTile {

        private const PICTS:Vector.<Class> = new <Class>[ tiles_01 ];

        public function CosmosTile( id:int ) {
            super ( id );
        }

        override public function draw():void {
            super.draw();
            _tile.desc.text = 'CosmosTile';
            drawBack( PICTS[0] );
        }
    }
}
