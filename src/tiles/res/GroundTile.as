/**
 * Created by Root on 05.12.17.
 */
package tiles.res {

    public class GroundTile extends BaseTile {

        private const PICTS:Vector.<Class> = new <Class>[ Ground ];

        public function GroundTile( id:int ) {
            super ( id );
        }

        override public function draw():void {
            super.draw();
            _tile.desc.text = 'GroundTile';
            drawBack( PICTS[0] );
        }
    }
}
