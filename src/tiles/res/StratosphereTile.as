/**
 * Created by Root on 05.12.17.
 */
package tiles.res {

    public class StratosphereTile extends BaseTile {

        private const PICTS:Vector.<Class> = new <Class>[ tiles_03, tiles_02 ];

        public function StratosphereTile( id:int) {
            super ( id );
        }

        override public function draw():void {
            super.draw();
            _tile.desc.text = 'StratosphereTile';
            drawBack( PICTS[_id] );
        }
    }
}
