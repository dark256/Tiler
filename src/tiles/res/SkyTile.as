/**
 * Created by Root on 05.12.17.
 */
package tiles.res {

    public class SkyTile extends BaseTile {

        private const PICTS:Vector.<Class> = new <Class>[ tiles_05, tiles_04 ];

        public function SkyTile( id:int ) {
            super ( id );
        }

        override public function draw():void {
            super.draw();
            _tile.desc.text = 'SkyTile';
            if (_id>=PICTS.length){
                drawBack( PICTS[ PICTS.length-1 ] );
            } else {
                drawBack( PICTS[_id] );
            }

        }
    }
}
