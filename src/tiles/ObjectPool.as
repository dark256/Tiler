/**
 * Created by Root on 06.12.17.
 */
package tiles {


    import tiles.res.BaseTile;

    public class ObjectPool {

        public static var _pool:Array = new Array();

        public static function poolIn( id:int, tile:BaseTile ):void{
            _pool[ id ] = tile;
        }

        public static function poolOut( id:int ):BaseTile{
            return _pool[ id ] ? _pool[ id ]:null;
        }

        public static function purge():void{
            for ( var i:int =0; i<_pool.length; i++ ){
                if ( _pool[i] ) {
                    _pool[ i ].purge();
                    _pool[ i ] = null;
                }
            }
        }
    }
}
