/**
 * Created by Root on 05.12.17.
 */
package tiles {

    import tiles.res.CosmosTile;
    import tiles.res.GroundTile;
    import tiles.res.SkyTile;
    import tiles.res.StratosphereTile;

    public class TileType {

        public static const GROUND:Class       = GroundTile;
        public static const SKY:Class          = SkyTile;
        public static const STRATOSPHERE:Class = StratosphereTile;
        public static const SPACE:Class        = CosmosTile;

    }
}
