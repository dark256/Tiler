
package tiles {

    import flash.display.MovieClip;
    import tiles.res.BaseTile;

    import tools.AdjustColor;

    public class TileManager {

        private const TILE_HEIGHT:int = 150;    // Высота тайла
        private const VIEW_SIZE:int = 2;        // Кол-во тайлов на экране

        private var _max_height:Number;          // Максимальная "высота" всей стопки тайлов
        private var _overlap:Number;            // "перехлёст" тайла за границу вьюпорта

        private var _viewPort:MainScreen;
        private var _target:MovieClip;

        private var _baseY:Number;
        private var _baseH:Number;

        private var _tileSet:Vector.<TileDescriptor> = new Vector.<TileDescriptor>();
        private var _ranges:Vector.<int> = new <int>[];

        private var _totalTiles:int = 0;
        private var _position:Number = 0;

        private var tileHash:Object = {};


        /**
         * TileManager
         * Created by dark256 on 05.12.17.
         * @param viewPort - вьюпорт для отрисовки тайлов
         * @param target - зона привязки тайлов
         * @param overlap - "перехлёст" тайла за границу вьюпорта
         */
        public function TileManager( viewPort:MainScreen, target:MovieClip, overlap:Number ) {
            _viewPort = viewPort;
            _target = target;
            _overlap = overlap;
            _ranges.push( 0 );
        }

        public function addTiles( tileClass:Class, count:int ):void {
            var td:TileDescriptor = new TileDescriptor();
            td.tileClass = tileClass;
            td.count = count;
            _tileSet.push( td );
            _totalTiles += count;
            _ranges.push( _totalTiles );
        }

        public function init():void {
            _baseY = _target.y;
            _baseH = _target.height;
            _max_height = (_totalTiles - VIEW_SIZE) * TILE_HEIGHT;
            trace( '■ TileManager.init TILES:', _totalTiles,' TARGET HEIGHT:', _max_height );
        }

        /**
         * Определяем тип тайла по его позиции в общем списке тайлов
         * @param pos
         * @return тип
         */
        public function getTileTypeAt( pos:int ):int {
            var ct:int = 0;
            while (_ranges[ ct ] <= pos && ct < _ranges.length - 1) {
                ct++;
            }
            return ct;
        }

        /**
         * Проверка на то, что для заданной позиции в списке тайлов у нас есть объекты отображения
         * @param val
         */
        private function checkPosition( val:int ):void {
            if ( _position + val < 0 ) {
                _position = 0;
            } else if ( _position + val > _max_height ) {
                _position = _max_height
            } else {
                _position += val;
            }
        }

        private function checkAndAddTile( id:int ):void {
            var tileObj:Object;
            if ( !tileHash[ id ] ) {
                tileObj = {tile:getTile( id ), id:id };
                tileHash[ id ] = tileObj;
                tileObj.tile.x = _target.x;
                //trace( '\t\tCREATE TAIL', tileObj.tile, tileObj.id );
            }
        }

        private function moveAndDelTile( q:String ):void {
            var tileObj:Object = tileHash[ q ];
            var tileYPosition:Number = _baseY + getPos( tileObj.id );

            var colorPos:Number = _position/_max_height;

            if ( tileYPosition > _baseY + _baseH + _overlap || tileYPosition < _baseY - TILE_HEIGHT -_overlap ) {
                //trace( '\t\tDELETE TILE', tileObj.tile, tileObj.id );
                tileObj.tile.destroy();
                delete tileHash[ q ];
            } else {
                tileObj.tile.y = tileYPosition;
                if ( colorPos>0.5 ){
                    AdjustColor.setDarkness( tileObj.tile, (0.5-colorPos)*512 );
                } else {
                    AdjustColor.setDarkness( tileObj.tile, 0 );
                }
            }

            _viewPort.inf.text = (_position/(_max_height/2)).toString();
        }

        public function drawViewPort( val:int = 0 ):void {

            checkPosition( val );

            var tileNum:Number = Math.floor( _position / TILE_HEIGHT );

            for ( var i:int = 0; i < 3; i++ ) { checkAndAddTile( tileNum + i ); }
            for ( var q:String in tileHash ) { moveAndDelTile( q ); }

            _viewPort.inf.appendText('\nPOS: ' + _position+' of '+_max_height);
            _viewPort.inf.appendText('\nFROM: '+tileNum+' TO: '+ ( tileNum+3 ) );
        }

        private function getPos( id:int ):Number {
            var pos:Number = _position - (id - 1) * TILE_HEIGHT;
            return pos;
        }

        private function getTile( pos:int ):BaseTile {

            var tileType:int = getTileTypeAt( pos ) - 1;
            var tileLocalPos:int = pos - _ranges[ tileType ];
            var poolObj:BaseTile = ObjectPool.poolOut( pos ) as BaseTile;
            var tile:BaseTile;

            if ( poolObj == null ) {
                var tileObj:BaseTile = new _tileSet[ tileType ].tileClass( tileLocalPos );
                tileObj.addInfo( ' pos: ' + pos + ' typ: ' + tileType + '\n[ ' + ( tileLocalPos + 1 ) + ' of ' + _tileSet[ tileType ].count + ' ]' );
                tile = _viewPort.addChild( tileObj ) as BaseTile;
                ObjectPool.poolIn( pos, tileObj );
                //trace('Create in Pool', tile, pos);
            } else {
                tile = _viewPort.addChild( poolObj ) as BaseTile;
                //trace('Get From Pool', tile, pos);
            }

            return tile;
        }

        private function clearViewPort():void {
            for each ( var tileObj:Object in tileHash ) {
                tileObj.tile.destroy();
            }
            tileHash = {};
        }
    }
}
