package {

    import flash.display.Sprite;
    import flash.events.*;
    import flash.net.FileFilter;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.utils.ByteArray;

    public class Main extends Sprite {
        private var fileRef:FileReference;

        public function Main() {
            fileRef = new FileReference();
            fileRef.addEventListener( Event.SELECT, onFileSelected );
            fileRef.addEventListener( Event.CANCEL, onCancel );
            fileRef.addEventListener( IOErrorEvent.IO_ERROR, onIOError );
            fileRef.addEventListener( SecurityErrorEvent.SECURITY_ERROR, onSecurityError );

            var textTypeFilter:FileFilter = new FileFilter( "Text Files (*.json)", "*.json");
            fileRef.browse( [ textTypeFilter ] );
        }

        public function onFileSelected( evt:Event ):void {
            fileRef.addEventListener( ProgressEvent.PROGRESS, onProgress );
            fileRef.addEventListener( Event.COMPLETE, onComplete );
            fileRef.load();
        }

        public function onProgress( evt:ProgressEvent ):void {
            trace( "Loaded " + evt.bytesLoaded + " of " + evt.bytesTotal + " bytes." );
        }

        public function onComplete( evt:Event ):void {
            trace( "File was successfully loaded." );
            //trace( fileRef.data );

            var dat:ByteArray = fileRef.data;
            //trace( '■ Main.onComplete' , dat.toString() );
            var obj:Object = JSON.parse( dat.readUTFBytes( dat.bytesAvailable ) );
            trace( obj );
        }

        public function onCancel( evt:Event ):void {
            trace( "The browse request was canceled by the user." );
        }

        public function onIOError( evt:IOErrorEvent ):void {
            trace( "There was an IO Error." );
        }

        public function onSecurityError( evt:Event ):void {
            trace( "There was a security error." );
        }
    }
}