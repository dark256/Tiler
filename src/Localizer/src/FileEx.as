package {

    import flash.display.Sprite;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.utils.ByteArray;

    public class FileEx extends Sprite {

        private const CR:String = '\r\n';       //String.fromCharCode(13)+String.fromCharCode(10);
        private const TB:String = '\t';

        public function FileEx() {

            var dat:ByteArray = readFile( new File( 'D:/MCASINO/GameKernel/bin/loc.json' ) );
            var obj:Object = JSON.parse( dat.readUTFBytes( dat.bytesAvailable ) );
            var str:String;

            str = '/**' + CR;
            str += '* Generated at ' + new Date() + ' by DHC Corp. LocoBot' + CR;
            str += '*/' + CR;
            str += 'package {' + CR + TB + 'public class LocKeys {' + CR + CR;

            var lines:Array = [];
            for ( var i:String in obj ) {
                lines.push( i );
            }
            lines.sort();

            for ( var j:int = 0; j < lines.length; j++ ) {
                if ( lines[ j ] == 'super' ) lines[ j ] = 'SUPER';
                str += TB + TB + "public static const " + lines[ j ] + ":String='" + lines[ j ] + "';" + CR;
            }

            str += CR + TB + '}' + CR + '}';

            var outputFile:File = new File( 'D:/MCASINO/GameKernel/src/LocKeys.as');
            writeFile( str, outputFile );
        }

        public function readFile( file:File, uncompress:String = null ):ByteArray {
            trace( '\t\t■ FileEx.readFile' );
            var result:ByteArray = new ByteArray();
            var inStream:FileStream = new FileStream();
            inStream.open( file, FileMode.READ );
            inStream.readBytes( result );
            inStream.close();
            result.position = 0;
            if ( uncompress ) {
                result.uncompress( uncompress );
            }
            return result;
        }

        public function writeFile( str:String, file:File, compress:String = null ):void {
            trace( '\t\t■ FileEx.writeFile' );
            var outStream:FileStream = new FileStream();
            outStream.open( file, FileMode.WRITE );
            outStream.writeUTFBytes( str );
            outStream.close();
            trace( '■ FileEx.DONE' );
        }

    }
}
